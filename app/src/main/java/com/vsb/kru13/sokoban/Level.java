package com.vsb.kru13.sokoban;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Level {
    @PrimaryKey(autoGenerate = true)
    public int uid;
    @ColumnInfo(name = "name")
    public String name;
    @ColumnInfo(name = "array")
    public int [] array;
    @ColumnInfo(name = "currentPosition")
    public int currentPosition;
    @ColumnInfo(name = "width")
    public int lx;
    @ColumnInfo(name = "height")
    public int ly;
    @ColumnInfo(name = "topScore")
    public int topScore;

    public Level() {
        array = new int[]{
                1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 0, 1, 0,
                1, 0, 2, 3, 3, 2, 1, 0, 1, 0,
                1, 0, 1, 3, 2, 3, 2, 0, 1, 0,
                1, 0, 2, 3, 3, 2, 4, 0, 1, 0,
                1, 0, 1, 3, 2, 3, 2, 0, 1, 0,
                1, 0, 2, 3, 3, 2, 1, 0, 1, 0,
                1, 0, 0, 0, 0, 0, 0, 0, 1, 0,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        };
        lx = 10;
        ly = 10;
        topScore = 0;
        name = "Default";
        currentPosition = 4*10+6;
    }
}

