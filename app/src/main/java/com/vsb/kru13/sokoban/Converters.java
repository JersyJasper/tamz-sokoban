package com.vsb.kru13.sokoban;

import androidx.room.TypeConverter;

public class Converters {
    @TypeConverter
    public static int[] fromString(String value) {
        int[] result = new int[value.length()];
        for (int i = 0; i < value.length(); i++) {
            result[i] = value.charAt(i) - '0';
        }
        return result;
    }

    @TypeConverter
    public static String fromArrayInts(int[] array) {
        String result = "";
        for (int i = 0; i < array.length; i++) {
            result += String.valueOf(array[i]);
        }
        return result;
    }
}
