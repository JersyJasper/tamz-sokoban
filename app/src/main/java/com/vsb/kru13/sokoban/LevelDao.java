package com.vsb.kru13.sokoban;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LevelDao {
    @Query("SELECT * FROM level")
    List<Level> getAll();

    @Query("SELECT * FROM level WHERE uid IN (:levelIds)")
    List<Level> loadAllByIds(int[] levelIds);

    @Query("SELECT * FROM level WHERE name = :pname")
    Level getLevelByName(String pname);

    @Query("SELECT * from level WHERE name= :pname")
    List<Level> getLevelsByName(String pname);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Level level);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Level... levels);

    @Update
    void update(Level level);

    @Query("UPDATE level SET array = :parray, currentPosition = :pcurrentPosition, topScore = :ptopScore WHERE name = :pname")
    void updateSpecific(String pname,  String parray, int pcurrentPosition, int ptopScore);


    @Delete
    void delete(Level user);

    @Query("DELETE FROM level")
    void nukeTable();

}

