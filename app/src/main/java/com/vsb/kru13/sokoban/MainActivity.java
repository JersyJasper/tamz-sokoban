package com.vsb.kru13.sokoban;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    SokoView sokoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sokoView = findViewById(R.id.sokoView);
        Intent intent = getIntent();
        Level lvl = new Level();
        lvl.array = intent.getIntArrayExtra("level");
        lvl.currentPosition = intent.getIntExtra("currentPosition", 0);
        lvl.lx = intent.getIntExtra("width", 0);
        lvl.ly = intent.getIntExtra("height", 0);
        lvl.topScore = intent.getIntExtra("topScore", 0);
        lvl.name = intent.getStringExtra("name");
        String title = lvl.name;
        setTitle(title);
        sokoView.setLevel(lvl);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                sokoView.ResetLevel();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
