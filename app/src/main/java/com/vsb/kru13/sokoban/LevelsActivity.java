package com.vsb.kru13.sokoban;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


public class LevelsActivity extends AppCompatActivity implements RecyclerLevelsAdapter.ItemClickListener {

    private static final int EMPTY = 0;
    private static final int WALL = 1;
    private static final int STAR = 3;
    private static final int BOX = 2;
    private static final int HERO = 4;
    private static final int BOXonSTAR = 5;
    private static final int HEROonSTAR = 6;
    private static final String DBNAME = "LevelsDB";


    LinkedHashMap<String, Level> parsedLevels = new LinkedHashMap<>();

    RecyclerLevelsAdapter adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DBNAME).allowMainThreadQueries().build();
        LevelDao levelDao = db.levelDao();

        setContentView(R.layout.activity_levels);
        recyclerView = findViewById(R.id.levels_list_recycleView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadAssetTextAsString("levels.txt");

        //levelDao.insert(parsedLevels.get("Level 1"));
        List<Level> databaseLevels = levelDao.getAll();
        for (int i = 0; i < databaseLevels.size(); i++) {
            parsedLevels.get(databaseLevels.get(i).name).topScore = databaseLevels.get(i).topScore;
        }

        adapter = new RecyclerLevelsAdapter(this, new ArrayList<Level>(parsedLevels.values()));
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);



    }

    @Override
    protected void onResume() {
        super.onResume();
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DBNAME).allowMainThreadQueries().build();
        LevelDao levelDao = db.levelDao();
        List<Level> databaseLevels = levelDao.getAll();
        for (int i = 0; i < databaseLevels.size(); i++) {
            parsedLevels.get(databaseLevels.get(i).name).topScore = databaseLevels.get(i).topScore;
        }
        adapter = new RecyclerLevelsAdapter(this, new ArrayList<Level>(parsedLevels.values()));
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {

        Level lvl = parsedLevels.get(adapter.getItem(position));
        Intent level_intent = new Intent(this, MainActivity.class);
        level_intent.putExtra("level", lvl.array);
        level_intent.putExtra("currentPosition", lvl.currentPosition);
        level_intent.putExtra("width", lvl.lx);
        level_intent.putExtra("height", lvl.ly);
        level_intent.putExtra("name", lvl.name);
        level_intent.putExtra("topScore", lvl.topScore);
        startActivity(level_intent);
    }

    private void loadAssetTextAsString(String fileName) {
        LinkedHashMap<String, ArrayList<String>> notParsedLevels = new LinkedHashMap<>();
        String levelName = "";
        ArrayList<String> levelString = new ArrayList<String>();
        boolean loadingLevel = false;
        BufferedReader in = null;
        try {
            InputStream inputStream = getAssets().open(fileName);
            in = new BufferedReader(new InputStreamReader(inputStream));
            String str;
            while ( (str = in.readLine()) != null ) {
                if (str.contains("Level"))
                {
                    levelName = str;
                    loadingLevel = true;
                }
                else if (str.contains("#"))
                {
                    levelString.add(str);

                }
                else if(str.isEmpty() && loadingLevel)
                {
                    notParsedLevels.put(levelName, levelString);
                    levelString = new ArrayList<>();
                    loadingLevel = false;
                    levelName = "";
                }
            }
            notParsedLevels.put(levelName, levelString);

        } catch (IOException e) {
            Log.e("Error", "Error opening asset " + fileName);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("Error", "Error closing asset " + fileName);
                }
            }
        }

        parseLevels(notParsedLevels);

    }

    private void parseLevels(LinkedHashMap<String, ArrayList<String>> notParsedLevels)
    {
        ArrayList<String> notParsedLevelsNames = new ArrayList<>(notParsedLevels.keySet());
        for (int i = 0; i < notParsedLevels.size(); i++) {
            Level tempLevel = parseStringToArray(notParsedLevels.get(notParsedLevelsNames.get(i)));
            tempLevel.name = notParsedLevelsNames.get(i);
            parsedLevels.put(tempLevel.name, tempLevel);
        }
    }
    private Level parseStringToArray(ArrayList<String> arrayList)
    {

        int longest = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            int temp = arrayList.get(i).length();
            if (temp > longest)
                longest = temp;
        }

        int level [] = new int[arrayList.size() * longest];
        int currentHeroPosition = 0;
        int index = 0;
        for (int i = 0; i < arrayList.size(); i++) {

            for (int j = 0; j < longest; j++) {
                if (j >= arrayList.get(i).length())
                {
                    level[index] = EMPTY;
                }
                else if (arrayList.get(i).charAt(j) == ' ')
                {
                    level[index] = EMPTY;
                }
                else if (arrayList.get(i).charAt(j) == '#')
                {
                    level[index] = WALL;
                }
                else if (arrayList.get(i).charAt(j) == '.')
                {
                    level[index] = STAR;
                }
                else if (arrayList.get(i).charAt(j) == '$')
                {
                    level[index] = BOX;
                }
                else if (arrayList.get(i).charAt(j) == '*')
                {
                    level[index] = BOXonSTAR;
                }
                else if (arrayList.get(i).charAt(j) == '@')
                {
                    level[index] = HERO;
                    currentHeroPosition = index;
                }
                else if (arrayList.get(i).charAt(j) == '+')
                {
                    level[index] = HEROonSTAR;
                    currentHeroPosition = index;
                }
                index++;
            }
        }

        Level result = new Level();
        result.array = level;
        result.currentPosition = currentHeroPosition;
        result.ly = arrayList.size();
        result.lx = longest;
        return result;
    }


}