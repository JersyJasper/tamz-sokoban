package com.vsb.kru13.sokoban;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerLevelsAdapter extends RecyclerView.Adapter<RecyclerLevelsAdapter.ViewHolder>{

    private List<Level> mData;
    private LayoutInflater layoutInflater;
    private ItemClickListener onItemClickListener;

    // data is passed into the constructor
    public RecyclerLevelsAdapter(Context context, List<Level> data) {
        this.layoutInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public RecyclerLevelsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowItem = layoutInflater.inflate(R.layout.levels_row, parent, false);
        return new ViewHolder(rowItem);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(RecyclerLevelsAdapter.ViewHolder holder, int position) {
        Level level = mData.get(position);
        holder.levelName.setText(level.name);
        holder.topScore.setText("Top score: " + String.valueOf(level.topScore));
        holder.sokoView.setPreview(level);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView levelName;
        private TextView topScore;
        private SokoView sokoView;


        public ViewHolder(View itemView) {
            super(itemView);
            this.levelName = itemView.findViewById(R.id.levelName_textView);
            this.topScore = itemView.findViewById(R.id.topScore_textView);
            this.sokoView = itemView.findViewById(R.id.preview_sokoView);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null)
            {
                onItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id).name;
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.onItemClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
