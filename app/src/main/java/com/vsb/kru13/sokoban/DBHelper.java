package com.vsb.kru13.sokoban;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/*
simple usage in MainActivity:

DBHelper dbHelper = new DBHelper(this);
dbHelper.insertContact("TAMZ");
ArrayList<String> myList = dbHelper.getAllCotacts();

ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1, myList);
ListView myL = findViewById(R.id.myList);
myL.setAdapter(arrayAdapter);

<ListView
    android:id="@+id/myList"
    android:layout_width="match_parent"
    android:layout_height="match_parent"/>

*/


public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Sokoban.db";
    public static final String CONTACTS_TABLE_NAME = "Levels";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "name";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + CONTACTS_TABLE_NAME + " (" +
                    CONTACTS_COLUMN_ID + " INTEGER PRIMARY KEY," +
                    CONTACTS_COLUMN_NAME + " TEXT)";

    public DBHelper(Context context){
        super(context,DATABASE_NAME,null,1);
    }

    public boolean insertContact (String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        db.insert(CONTACTS_TABLE_NAME, null, contentValues);
        return true;
    }

    public ArrayList<String> getAllCotacts() {
        ArrayList<String> array_list = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from CONTACTS_TABLE_NAME", null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            array_list.add(res.getString(res.getColumnIndexOrThrow(CONTACTS_COLUMN_NAME)));
            res.moveToNext();
        }
        return array_list;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}