package com.vsb.kru13.sokoban;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.room.Room;

import java.util.List;

public class SokoView extends View{

    private static final int EMPTY = 0;
    private static final int WALL = 1;
    private static final int STAR = 3;
    private static final int BOX = 2;
    private static final int HERO = 4;
    private static final int BOXonSTAR = 5;
    private static final int HEROonSTAR = 6;
    private static final String DBNAME = "LevelsDB";

    SokoView sokoView;

    Level level;

    Bitmap[] bmp;

    boolean already_won = false;

    int currentPosition;
    int backup_currentPosition;

    int moves = 0;

    int width;
    int height;

    private int backup_level[];

    public SokoView(Context context) {
        super(context);
        sokoView = findViewById(R.id.sokoView);
        level = new Level();
        MakeBackupArray(level.array);
        currentPosition = level.currentPosition;
        backup_currentPosition = level.currentPosition;
        init(context);
    }

    public SokoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        sokoView = findViewById(R.id.sokoView);
        level = new Level();
        MakeBackupArray(level.array);
        currentPosition = level.currentPosition;
        backup_currentPosition = level.currentPosition;
        init(context);
    }

    public SokoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        sokoView = findViewById(R.id.sokoView);
        level = new Level();
        MakeBackupArray(level.array);
        currentPosition = level.currentPosition;
        backup_currentPosition = level.currentPosition;
        init(context);
    }

    void init(Context context) {
        bmp = new Bitmap[7];

        bmp[0] = BitmapFactory.decodeResource(getResources(), R.drawable.empty);
        bmp[1] = BitmapFactory.decodeResource(getResources(), R.drawable.wall);
        bmp[2] = BitmapFactory.decodeResource(getResources(), R.drawable.box);
        bmp[3] = BitmapFactory.decodeResource(getResources(), R.drawable.goal);
        bmp[4] = BitmapFactory.decodeResource(getResources(), R.drawable.hero);
        bmp[5] = BitmapFactory.decodeResource(getResources(), R.drawable.boxok);
        bmp[6] = BitmapFactory.decodeResource(getResources(), R.drawable.hero);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        width = w / level.lx;
        height = h / level.ly;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        for (int i = 0; i < level.ly; i++) {
            for (int j = 0; j < level.lx; j++) {
                canvas.drawBitmap(bmp[level.array[i*level.lx + j]], null,
                        new Rect(j*width, i*height,(j+1)*width, (i+1)*height), null);
            }
        }

    }

    float firstX_point, firstY_point;
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getAction();

        switch (action) {

            case MotionEvent.ACTION_DOWN:
                firstX_point = event.getRawX();
                firstY_point = event.getRawY();
                break;

            case MotionEvent.ACTION_UP:

                float finalX = event.getRawX();
                float finalY = event.getRawY();

                int distanceX = (int) (finalX - firstX_point);
                int distanceY = (int) (finalY - firstY_point);

                if (Math.abs(distanceX) > Math.abs(distanceY)) {
                    if ((firstX_point < finalX)) {
                        MoveRight();
                        //Log.d("Test", "Left to Right swipe performed");
                    } else {
                        MoveLeft();
                        //Log.d("Test", "Right to Left swipe performed");
                    }
                }else{
                    if ((firstY_point < finalY)) {
                        MoveDown();
                        //Log.d("Test", "Up to Down swipe performed");
                    } else {
                        MoveUp();
                        //Log.d("Test", "Down to Up swipe performed");
                    }
                }


                break;
        }

        return true;
    }

    public void setPreview(Level newLevel)
    {
        this.level = newLevel;
        MakeBackupArray(level.array);
        currentPosition = newLevel.currentPosition;
        backup_currentPosition = newLevel.currentPosition;

        sokoView = findViewById(R.id.preview_sokoView);
        sokoView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return true;
            }
        });
        sokoView.invalidate();
    }

    public void setLevel(Level newLevel)
    {
        MakeBackupArray(newLevel.array);
        backup_currentPosition = newLevel.currentPosition;
        AppDatabase db = Room.databaseBuilder(getContext(),
                AppDatabase.class, DBNAME).allowMainThreadQueries().build();

        LevelDao levelDao = db.levelDao();
        Level databaseLevel = levelDao.getLevelByName(newLevel.name);
        if (databaseLevel != null)
        {
            newLevel = databaseLevel;
        }

        this.level = newLevel;
        currentPosition = newLevel.currentPosition;
        sokoView = findViewById(R.id.sokoView);
        sokoView.invalidate();
        boolean incomplete = contains(level.array, BOX);
        if (!incomplete)
        {
            already_won = true;
        }
    }

    private void MakeBackupArray(int array[])
    {
        backup_level = new int[array.length];
        System.arraycopy(array, 0 , backup_level, 0, array.length);
    }

    private void MoveRight()
    {
        if((currentPosition/level.lx) > level.lx)
        {
            return;
        }

        Move(+1);

    }

    private void MoveLeft()
    {
        if((currentPosition-1) < 0)
        {
            return;
        }

        Move(-1);

    }

    private void MoveDown()
    {
        if((currentPosition+level.lx) >= (level.lx*level.ly))
        {
            return;
        }

        Move(+level.lx);
    }

    private void MoveUp()
    {
        if((currentPosition-level.lx) < 0)
        {
            return;
        }

        Move(-level.lx);

    }

    private void Move(int number)
    {
        if (level.array[currentPosition+(number)] == WALL)
        {
            return;
        }
        if (level.array[currentPosition+(number)] == BOX || level.array[currentPosition+(number)] == BOXonSTAR)
        {
            if (level.array[currentPosition+((number)*2)] == BOX || level.array[currentPosition+((number)*2)] == BOXonSTAR || level.array[currentPosition+((number)*2)] == WALL)
            {
                return;
            }
            else
            {
                if(level.array[currentPosition+((number)*2)] == STAR)
                {
                    level.array[currentPosition+((number)*2)] = BOXonSTAR;

                }
                else if (level.array[currentPosition+((number)*2)] == EMPTY)
                {
                    level.array[currentPosition+((number)*2)] = BOX;
                }

            }

        }

        if (backup_level[currentPosition] == STAR || backup_level[currentPosition] == BOXonSTAR ||backup_level[currentPosition] == HEROonSTAR || level.array[currentPosition] == HEROonSTAR)
        {
            level.array[currentPosition] = STAR;

        }
        else
        {
            level.array[currentPosition] = EMPTY;
        }
        currentPosition = currentPosition +(number);
        level.array[currentPosition] = HERO;
        moves++;
        CheckIfLevelComplete();

    }

    private boolean contains(int [] array, int v) {
        for (int e : array)
            if (e == v)
                return true;
        return false;
    }

    private void CheckIfLevelComplete()
    {
        AppDatabase db = Room.databaseBuilder(getContext(),
                AppDatabase.class, DBNAME).allowMainThreadQueries().build();
        LevelDao levelDao = db.levelDao();

        boolean incomplete = contains(level.array, BOX);

        sokoView.invalidate();
        if (incomplete)
        {
            level.currentPosition = currentPosition;
            List<Level> levelsFromDB = levelDao.getLevelsByName(level.name);
            if (levelsFromDB.isEmpty())
                levelDao.insert(level);
            else

                levelDao.updateSpecific(level.name, Converters.fromArrayInts(level.array), level.currentPosition, level.topScore);
        }
        else
        {

            if (!already_won)
            {
                Win();
                if (level.topScore > moves || level.topScore == 0)
                {
                    level.topScore = moves;
                }
                already_won = true;
            }

            level.currentPosition = currentPosition;
            List<Level> levelsFromDB = levelDao.getLevelsByName(level.name);
            if (levelsFromDB.isEmpty())
                levelDao.insert(level);
            else
                levelDao.updateSpecific(level.name, Converters.fromArrayInts(level.array), level.currentPosition, level.topScore);


        }
    }

    public void Win()
    {
        //TODO win next level
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("LEVEL COMPLETED");
        builder.setMessage("Choose a next level");
        builder.setCancelable(true);
        builder.setPositiveButton("Levels", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(getContext(), LevelsActivity.class);
                getContext().startActivity(intent);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Close Dialog
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
    public void ResetLevel()
    {
        System.arraycopy(backup_level, 0, level.array, 0, backup_level.length);
        currentPosition = backup_currentPosition;
        already_won = false;
        moves = 0;
        sokoView.invalidate();

    }

}
